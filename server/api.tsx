import axios from "axios";
// eslint-disable-next-line import/no-cycle
import { Loan } from "../src/components/Modal";

axios.defaults.baseURL = "http://localhost:3004";

export const getLoans = async () => (await axios.get('/loans')).data;

export const addInvest = async (loan: Loan) => (await axios.put(`/loans/${loan.id}`, loan)).data


