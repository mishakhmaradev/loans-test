import React from 'react';
import { render, screen, within } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import Modal from './Modal';

const setState = jest.fn();

const loan = {
  id: '1',
  title: 'Voluptate et sed tempora qui quisquam.',
  tranche: 'A',
  available: '11959',
  term_remaining: '1655351173',
  ltv: '48.80',
  amount: '85754',
};

describe('close modal', () => {
  it('should not render modal before click', async () => {
    const modalContainer = screen.queryByTestId('modal');
    expect(modalContainer).not.toBeInTheDocument();
  });

  it('renders Modal component given the props', () => {
    render(<Modal setModalOpen={setState} loan={loan} />);

    const modal = screen.getByTestId('modal');
    expect(modal).toBeInTheDocument();

    const title = screen.getByTestId('title');
    expect(title).toBeInTheDocument();
    expect(title).toHaveTextContent('Voluptate et sed tempora qui quisquam.');

    const amountAvailable = screen.getByTestId('available');
    expect(amountAvailable).toHaveTextContent('£11,959.00');

    const loansEndsIn = screen.getByTestId('loansEndsIn');
    expect(loansEndsIn).toHaveTextContent('1 month 2 days');

    const investmentAmount = screen.getByTestId('investmentAmount');
    expect(investmentAmount).toHaveTextContent('Investment amount (£)');

    const input = screen.getByTestId('form');
    expect(input).toBeInTheDocument();

    const button = screen.getByTestId('button-submit');
    expect(button).toBeInTheDocument();
  });
});

describe('Modal input validation', () => {
  it('should render error message with too big number in input', async () => {
    render(<Modal setModalOpen={setState} loan={loan} />);
    const modalContainer = screen.getByTestId('modal');
    const modalInput = await within(modalContainer).getByTestId('input');
    const submitBtn = within(modalContainer).getByRole('button', { name: /invest/i });

    userEvent.type(modalInput, '12000');
    expect(modalInput).toHaveValue(12000);
    userEvent.click(submitBtn);
    expect(await screen.findByTestId('error')).toHaveTextContent(
      'invest must be less than or equal to 11959'
    );
  });
});
