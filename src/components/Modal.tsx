/* eslint-disable no-param-reassign */
/* eslint-disable @typescript-eslint/no-explicit-any */

import React from 'react';
import { Formik, Field, Form, ErrorMessage } from 'formik';
import styled from 'styled-components';
import * as yup from 'yup';
import { format } from './formatter';
// eslint-disable-next-line import/no-cycle
import { addInvest } from '../../server/api';

const Button = styled.button`
  width: 120px;
  height: 45px;
  background-color: yellow;
  border: none;
`;

const ModalBackground = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  position: absolute;
  z-index: 1;
  width: 100%;
  height: 100%;
  left: 0px;
  top: 0px;
  background-color: rgba(16, 16, 18, 0.4);
`;

const ModalWrapper = styled.div`
  padding: 10px;
  width: 450px;
  height: 280px;
  border-radius: 5px;
  background-color: #faf8f8;
`;

const StyledField = styled(Field)`
  margin-right: 10px;
  height: 26px;
  width: 300px;
  height: 25px;
  border-radius: 4px;
  padding: 10px;
  border: none;
`;

const StyledFormik = styled(Formik)`
  display: flex;
`;

export const StyledErrorMessage = styled(ErrorMessage)`
  position: absolute;
  margin-top: 8px;
  font-size: 11px;
  line-height: 12px;
  color: red;
`;
export interface Loan {
  id: string;
  title: string;
  tranche: string;
  available: string;
  term_remaining: string;
  ltv: string;
  amount: string;
}
interface Props {
  setModalOpen: (modalOpen: boolean) => void;
  loan: Loan;
}

const Modal: React.FC<Props> = ({ setModalOpen, loan }) => {
  const available = Number(loan.available);
  const amount = Number(loan.amount);

  const schema = yup.object().shape({
    invest: yup.number().max(available).required().positive().integer(),
  });

  const date = Number(loan.term_remaining);
  const date1 = new Date(date);
  const month = date1.getMonth() + 1;
  const day = date1.getDay();

  const nowAvailable = format.format(Number(available));

  const initialValues = {
    invest: '',
  };

  const onInvest = (value: number) => {
    const leftAvailable = available - value;
    const personAvailable = JSON.stringify(leftAvailable);
    loan.available = personAvailable;

    const leftAmount = amount + value;
    const personAmount = JSON.stringify(leftAmount);
    loan.amount = personAmount;
  };

  return (
    <ModalBackground
      data-testid="modal-close"
      className="modal"
      onClick={(e) => {
        if (e.currentTarget === e.target) {
          setModalOpen(false);
        }
      }}
    >
      <ModalWrapper data-testid="modal">
        <h2>Invest in Load</h2>
        <p data-testid="title">{loan.title}</p>
        <p data-testid="available">{`Amount available: ${nowAvailable}`}</p>
        <p data-testid="loansEndsIn">{`Loans ends in: ${month} month ${day} days`}</p>
        <p data-testid="investmentAmount">Investment amount (£)</p>
        <StyledFormik
          initialValues={initialValues}
          validationSchema={schema}
          onSubmit={(value) => {
            onInvest(value.invest);
            addInvest(loan);
            setModalOpen(false);
          }}
        >
          <Form data-testid="form">
            <StyledField data-testid="input" name="invest" type="number" />
            <StyledErrorMessage data-testid="error" name="invest" component="div" />
            <Button data-testid="button-submit" type="submit">
              INVEST
            </Button>
          </Form>
        </StyledFormik>
      </ModalWrapper>
    </ModalBackground>
  );
};

export default Modal;
