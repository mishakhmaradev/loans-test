import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { getLoans } from '../../server/api';
import { format } from './formatter';
import Modal, { Loan } from './Modal';

export const InvestButton = styled.button`
  width: 100px;
  height: 35px;
  background-color: yellow;
  margin-top: 25px;
`;

const LoansWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
`;

const CurrentLoans = styled.div`
  padding: 30px;
  background-color: #d1cece;
  width: 600px;
  height: 470px;
`;

const LoansTitle = styled.h1`
  font-size: 20px;
  color: #1f1d1d;
`;

const LoanCard = styled.div`
  display: flex;
  justify-content: space-between;
  padding: 10px;
  margin-bottom: 15px;
  width: 96%;
  height: 80px;
  border: 1px solid;
  background-color: white;
`;

const CardTitle = styled.h2`
  font-size: 16px;
`;

const InvestedWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`;

const Invested = styled.p`
  margin: 0px;
  color: green;
`;

const TotalAvailable = styled.p`
  padding-top: 30px;
  text-align: center;
  width: 350px;
  margin: 0 auto;
`;

const Loans: React.FC = () => {
  const [modalOpen, setModalOpen] = useState(false);
  const [currentLoan, setCurrentLoan] = useState<Loan>();
  const [loansData, setLoans] = useState<Loan[]>([]);
  const [invested, setInvested] = useState<{ [key: string]: boolean }>({});

  useEffect(() => {
    getLoans().then((data) => setLoans(data));
  }, []);

  const totalAvailable = loansData.reduce(
    (previousValue, currentValue) => Number(previousValue) + Number(currentValue.available),
    0
  );

  const onInvest = (loan: Loan) => {
    invested[loan.id] = true;
    setInvested(invested);
    setCurrentLoan(loan);
    setModalOpen(!modalOpen);
  };

  return (
    <LoansWrapper>
      <CurrentLoans>
        <LoansTitle data-testid="LoansTitle">Current Loans</LoansTitle>
        {loansData.map((loan: Loan) => (
          <LoanCard key={loan.id} data-testid={`templateCard-${loan.id}`}>
            <div>
              <CardTitle>{loan.title}</CardTitle>
              <p data-testid="loanDetails">
                {`Loans details: amount:  ${format.format(Number(loan.amount))} 
              , available: ${format.format(Number(loan.available))} `}
              </p>
            </div>
            <InvestedWrapper>
              {invested[loan.id] && <Invested data-testid="InvestedLabel">invested</Invested>}

              <InvestButton value={loan.id} type="button" onClick={() => onInvest(loan)}>
                INVEST
              </InvestButton>
              {modalOpen && currentLoan && <Modal setModalOpen={setModalOpen} loan={currentLoan} />}
            </InvestedWrapper>
          </LoanCard>
        ))}

        <TotalAvailable data-testid="totalAvailable">{`Total amount available for investments:  
        ${format.format(Number(totalAvailable))}`}</TotalAvailable>
      </CurrentLoans>
    </LoansWrapper>
  );
};
export default Loans;
