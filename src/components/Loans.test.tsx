import React from 'react';
import { render, screen, waitFor, within } from '@testing-library/react';
import Loans from './Loans';

describe('Loans', () => {
  it('should render Loans component', async () => {
    render(<Loans />);
    const loansTitle = screen.getByTestId('LoansTitle');
    expect(loansTitle).toBeInTheDocument();
    expect(loansTitle).toHaveTextContent('Current Loans');

    const totalAvailable = await screen.getByTestId('totalAvailable');

    await waitFor(() => {
      expect(totalAvailable).toBeInTheDocument();
    });
  });

  it('should render three cards of loans', async () => {
    render(<Loans />);
    await waitFor(() => {
      screen.getByTestId('templateCard-1');
    });
    const card = screen.getByTestId('templateCard-1');

    const loanTitle = within(card).getByRole('heading', {
      name: 'Voluptate et sed tempora qui quisquam.',
    });
    expect(loanTitle).toBeInTheDocument();

    const loanDetails = within(card).getByTestId('loanDetails');
    expect(loanDetails).toBeInTheDocument();
    const loanButton = within(card).getByRole('button', { name: /invest/i });
    expect(loanButton).toBeInTheDocument();
  });
});
