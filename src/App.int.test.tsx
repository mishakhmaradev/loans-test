import React from 'react';
import { render, screen, waitFor, within } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import userEvent from '@testing-library/user-event';
import '@testing-library/jest-dom';
import App from './App';

const server = setupServer(
  rest.get('http://localhost:3004/loans', (req, res, ctx) =>
    res(
      ctx.json([
        {
          id: '1',
          title: 'Voluptate et sed tempora qui quisquam.',
          tranche: 'A',
          available: '11959',
          annualized_return: '8.60',
          term_remaining: '1655351173',
          ltv: '48.80',
          amount: '85754',
        },
        {
          id: '5',
          title: 'Consectetur ipsam qui magnam minus dolore ut fugit.',
          tranche: 'B',
          available: '31405',
          annualized_return: '7.10',
          term_remaining: '1644986773',
          ltv: '48.80',
          amount: '85754',
        },
      ])
    )
  )
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

describe('App integration test', () => {
  it('should pass happy pass', async () => {
    render(<App />);
    await waitFor(() => {
      screen.getByTestId('templateCard-1');
    });
    const loanContainer = screen.getByTestId('templateCard-1');
    const loanBtn = within(loanContainer).getByRole('button', { name: /invest/i });
    userEvent.click(loanBtn);
    const modalInput = await within(loanContainer).getByTestId('input');
    expect(modalInput).toBeInTheDocument();

    screen.debug();
    userEvent.type(modalInput, '3000');
    expect(modalInput).toHaveValue(3000);

    const modalContainer = within(loanContainer).getByTestId('modal');
    const submitBtn = within(modalContainer).getByRole('button', { name: /invest/i });
    userEvent.click(submitBtn);

    expect(await within(loanContainer).findByTestId('modal')).not.toBeInTheDocument();

    const loanAmount = within(loanContainer).getByTestId('loanDetails');
    expect(loanAmount).toHaveTextContent(
      'Loans details: amount: £88,754.00 , available: £8,959.00'
    );

    const totalAvailable = screen.getByTestId('totalAvailable');
    expect(totalAvailable).toHaveTextContent('£40,364.00');
  });

  it('should not render modal if closed', async () => {
    render(<App />);
    await waitFor(() => {
      screen.getByTestId('templateCard-1');
    });
    const loanContainer = screen.getByTestId('templateCard-1');
    const loanBtn = within(loanContainer).getByRole('button', { name: /invest/i });
    userEvent.click(loanBtn);
    const modalContainer = within(loanContainer).getByTestId('modal');
    expect(modalContainer).toBeInTheDocument();
    const closeModal = within(loanContainer).getByTestId('modal-close');
    expect(closeModal).toBeInTheDocument();
    userEvent.click(closeModal);
    expect(screen.queryByTestId('modal')).not.toBeInTheDocument();
  });
});
